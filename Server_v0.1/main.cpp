#include <stdio.h>
#include <locale.h>
#include <winsock2.h> 
#include <windows.h>
#include <string.h>
#include <time.h>


#define NUM_CLIENTS		10
#define PORT			443
#define BUFFER			1024
#define LEN_NICK		255


// �������� �������, ������������� �������������� �������������
DWORD WINAPI	ThClient(LPVOID ID);
HANDLE			hThread;
SOCKET			client_socket; //����� ��� �������
SOCKADDR_IN		sin;
SOCKADDR_IN		client_address; //����� �������
int				NClient = 0,
				ClientsOnline = 0;
char			lastMess[50];

/*
�������� ������� � 3� ����������
__________________________________
|_nick___|_ip____|_sock__|_live__|
|toxbh   | 1.1.1 | 132   | 1     | 1 ������ .
|vasya   | 1.2.3 | 54    | 0     | ����� - ����� ������.
|artacer | 2.2.2 | 164   | 1     | 1 ������ artacer
|artacer | 2.2.3 | 172   | 1     | 2 ������ artacer
|________|_______|_______|_0_____| ������ ������

*/
char			ClientNickIp[NUM_CLIENTS][2][LEN_NICK];
SOCKET			ClientSock[NUM_CLIENTS];
int				ClientLive[NUM_CLIENTS];

/*
	��������� � ������ ������ ������������
	���������� id ������� � ������
*/
int NewClient(char* nick, char* ip, SOCKET sock) {
	int		number_client,
			i;
	bool	inLive = false;

	//��������� ��� �� ������ ������ 
	for (i = 0; i < NClient; i++) {
		if (ClientLive[i] == 0) {
			int m;
			for (m = 0; m < (int)strlen(nick); m++) {//set Nick
				ClientNickIp[i][0][m] = nick[m];
			}

			for (int m = 0; m < (int)strlen(ip); m++) {//set IP
				ClientNickIp[i][1][m] = ip[m];
			}
			ClientSock[i] = sock;
			ClientLive[i] = 1;
			inLive = true;
			number_client = i;
			ClientsOnline++;
			break;
		}
	}

	//���� �� ����� �����. ��������� �����
	if (!inLive) {
		if (ClientLive[NClient] == 1)
			NClient++;

		//��������, ��� �� ��������� ���� ������ ��������
		if (NClient < NUM_CLIENTS) {
			for (i = 0; i < (int)strlen(nick); i++) {
				ClientNickIp[NClient][0][i] = nick[i];
			}

			for (int i = 0; i < (int)strlen(ip); i++) {
				ClientNickIp[NClient][1][i] = ip[i];
			}
			ClientSock[NClient] = sock;
			ClientLive[NClient] = 1;
			number_client = NClient;
			ClientsOnline++;
		}
		else {
			return 0;
		}
	}

	
	return number_client;
}

//������� �������
int DeleteClient(int id) {
	ClientsOnline--;
	//���� id ������ ���-�� ������ ������ �� ������
	if (id > NClient) {
		return 0;
	}
	//���� ������������ ��� ������ ���� ��������
	if (ClientLive[id] == 0) {
		return 0;
	}
	//�������� ����� ������������
	ClientLive[id] = 0;
	ClientSock[id] = 0;
	memset(ClientNickIp[id][0], 0, LEN_NICK);
	memset(ClientNickIp[id][1], 0, LEN_NICK);

	closesocket(ClientSock[id]);

	return 0;
}

//�������� ������ �� �������
void ViewTable() {
	printf("\n         USERS ONLINE: %d\n|_nick________|_ip_______________|_sock___|_live__|\n", ClientsOnline);
	for (int i = 0; i <= NClient; i++) {
		printf("| %-10s  | %-15s  | %-5d  | %-4d  |\n", ClientNickIp[i][0], ClientNickIp[i][1], (int)ClientSock[i], ClientLive[i]);
	}
}

// 0 - ���������; 1 - ������ ����; 9 - ����������� �� �������.
void SendOnlineList(char* OnlineList) {
	OnlineList[0] = '1';
	int x = 1;
	for (int i = 0; i <= NClient; i++) {
		if (ClientLive[i] == 1) {
			char* Adres = ClientNickIp[i][1];
			int lenAdr = strlen(Adres);
			for (int j = 0; j < lenAdr; j++) {
				OnlineList[x] = Adres[j];
				x++;
			}
			OnlineList[x] = '\n';
			x++;
		}
	}
	OnlineList[x] = '\0';
	printf("%s", OnlineList);
}

//������������ �����
char* sumstr(char* first, char* second) {
	int flen = strlen(first);
	int slen = strlen(second);
	int allLen = flen + slen;

	char* out = new char[allLen + 1];

	for (int i = 0; i <= allLen; i++) {
		if (i < flen) {
			out[i] = first[i];
		}
		else {
			out[i] = second[i - flen];
		}
	}

	out[allLen + 1] = '\0';

	return out;
}



int main(int argc, char* argv[]) {
	setlocale(LC_ALL, ".OCP");
	printf("GurrenServer Statring\n");

	//�������� ������ ������ 2.2 � ������� ��������� WSADATA
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData)) {
		printf("Error WSASturtup %d\n", WSAGetLastError());
		return 1;
	}

	//�������� �� dll 
	if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
		printf("Could not find a usable version of Winsock.dll\n");
		WSACleanup();
		return 1;
	}
	else
		printf("The Winsock 2.2 dll was found okay\n");

	// �������� ������
	printf("Create socket\n");
	SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
	if (sock == INVALID_SOCKET) {
		printf("Could not find a usable version of Winsock.dll\n");
		WSACleanup();
		return 1;
	}

	// ������� ����� �� ����
	printf("Ready socket\n");
	sin.sin_family = AF_INET;
	sin.sin_port = htons(PORT);
	sin.sin_addr.s_addr = INADDR_ANY;

	// ����������� � ����� � ��������� ��� ����������
	if (bind(sock, (SOCKADDR *)&sin, sizeof(sin)) == SOCKET_ERROR) {
		printf("Error bind %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}

	// ������� ����, ������� 8
	listen(sock, 8);

	printf("Wait conection...\n");

	while (1) {
		//����������� ������
		int client_addres_size = sizeof(client_address); 
		client_socket = accept(sock, (SOCKADDR *)&client_address, &client_addres_size);
		if (client_socket == INVALID_SOCKET)
		{
			printf("accept() failed: %d\n", WSAGetLastError());
			return 1;

		}
		printf("Accepted client: %s:%d\n",
			inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port));

		char buff[512];
		int ret = recv(client_socket, buff, sizeof(buff), 0);
		if (ret == 0)
			break;
		else if (ret == SOCKET_ERROR)
		{
			printf("recv() failed 235: %d\n",
				WSAGetLastError());
			//break;
		}
		printf("%s\n", buff);
		//delete buff;

		if (buff[0] == '9') {
			char nick[255];
			int i;
			for (i = 1; i < (int)strlen(buff) + 1; i++) {
				nick[i - 1] = buff[i];
			}
			nick[i] = '\0';
			printf("nick: %s\n", nick);

			int id = NewClient(nick, inet_ntoa(client_address.sin_addr), client_socket);
			
			ViewTable();

			char* online;

			if (ClientsOnline > 0) {
				printf("%d user online\n", ClientsOnline);

				char OnlineList[1024];
				SendOnlineList(OnlineList);
				for (i = 0; i <= NClient; i++) {
					if (ClientLive[i] == 1) {
						send(ClientSock[i], OnlineList, strlen(OnlineList) + 1, 0);
					}
				}
			}

			else {
				printf("No User on line\n");
			}


			DWORD thID;
			hThread = CreateThread(NULL, NULL, ThClient, (LPVOID)id, NULL, &thID);
			if (hThread == NULL)
			{
				printf("CreateThread() failed: %d\n", GetLastError());
				break;
			}
			CloseHandle(hThread);
		}
		else  {
			closesocket(client_socket);
		}
	}

	closesocket(client_socket);
	WSACleanup();

	return 0;
}

// ��� ������� ��������� � ��������� ������
// � ���������� ���������� ��������������� ������� ���������� �� ���������
DWORD WINAPI ThClient(LPVOID ID)
{

	SOCKET 	mySocket;
	char 	buff[BUFFER],
			myNick[20];
	int 	myID = (int)ID,
			ret,
			i;

	mySocket = ClientSock[myID];
	memset(&buff, 0, 1024);
	//�������� ����� ���� � ����������
	strcpy_s(myNick, strlen(ClientNickIp[NClient][0]) + 1, ClientNickIp[NClient][0]);

	while (1) {
		ret = recv(mySocket, buff, BUFFER, 0);
		if (ret == 0)
			break;
		else if (ret == SOCKET_ERROR)
		{
			printf("recv() failed 235: %d\n",
				WSAGetLastError());
			break;
		}
		buff[ret] = '\0';
		printf("Mess from %s", myNick);

		time_t current_time;
		struct tm time_info;
		char timeString[6];
		time(&current_time);
		localtime_s(&time_info, &current_time);
		strftime(timeString, 6, "%H:%M", &time_info);

		if (strcmp(lastMess, myNick) != 0) {
			for (i = 0; i <= NClient; i++) {
				if (ClientLive[i] == 1) {
					char* BMC = sumstr("0", sumstr(timeString,
						sumstr(
						sumstr(" [", (char*)myNick),
						sumstr(
						"]: ",
						buff))));
					printf("%s", BMC);
					ret = send(ClientSock[i], BMC, strlen(BMC) + 1, 0);
					if (ret == 0) 
						break;					
					else if (ret == SOCKET_ERROR)
					{
						printf("send() failed 263: %d\n",
							WSAGetLastError());
						break;
					}
				}
			}
		}
		else {
			for (i = 0; i <= NClient; i++) {
				if (ClientLive[i] == 1) {
					char* BMC = sumstr("0", sumstr(timeString,
						sumstr(": ", buff)));
					printf("%s", BMC);
					ret = send(ClientSock[i], BMC, strlen(BMC) + 1, 0);
					if (ret == 0){
						DeleteClient(i);
					}
					else if (ret == SOCKET_ERROR)
					{
						printf("send() failed 280: %d\n",
							WSAGetLastError());
						closesocket(ClientSock[i]);
						break;
					}
				}
			}
		}

		strcpy_s(lastMess, 50, myNick);
		memset(&buff, 0, 1024);
	}

	DeleteClient(myID);
	printf("Disconect\n");
	if (NClient > 0) {
		char OnlineList[1024];
		SendOnlineList(OnlineList);
		for (i = 0; i <= NClient; i++) {
			if (ClientLive[i] == 1) {
				ret = send(ClientSock[i], OnlineList, strlen(OnlineList) + 1, 0);
				if (ret == 0)
					return 1;
				else if (ret == SOCKET_ERROR)
				{
					printf("send() failed 216: %d\n",
						WSAGetLastError());
					return 1;
				}
			}
		}
	}
	ViewTable();

	return 0;
}